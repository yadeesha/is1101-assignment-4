/*C program to convert celsius into fahrenheit*/
#include <stdio.h>
int main(){

	float cel, fahr;
	printf("Enter temperature in Celsius: ");
	scanf("%f", &cel);
	
	fahr=(cel * 9/5)+32;
	printf("Given temperature in Fahrenheit: %.2f\n", fahr);
return 0;
}
