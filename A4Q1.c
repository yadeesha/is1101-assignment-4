/*c program to calculate the sum and avg of three integers*/
#include <stdio.h>
int main(){
 
	int num1, num2, num3, sum, avg;
	printf("Enter any three integers: ");
	scanf("%d %d %d", &num1, &num2, &num3);
	
	sum = num1 + num2 + num3;
	avg= sum/3;

	printf("Sum of the three numbers: %d\n",sum);
	printf("Average of the three numbers: %d\n", avg);
return 0;
}
