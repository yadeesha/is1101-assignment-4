/*C program to swap numbers without temporary variable*/
#include <stdio.h>
int main(){

	int num1, num2;
	printf("Enter two numbers to be swapped:\n ");
	printf("First number: ");
	scanf("%d", &num1);
	printf("Second number: ");
	scanf("%d", &num2);

	num1= num1 - num2;
	num2= num1 + num2;
	num1= num2 - num1;
	
	printf("Numbers after being swapper: \n");
	printf("First number: %d\n", num1);
	printf("Second number: %d\n", num2);

return 0;
}
