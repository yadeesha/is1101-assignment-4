/*C program demonstrate bitwise operators*/
#include <stdio.h>
int main(){
	int A =10, B=15;

	printf("AND output (A&B) = %d\n", A&B);
	printf("XOR output (A^B) = %d\n", A^B);
	printf("Complement output (~A) = %d\n", ~A);
	printf("Left shift output (A<<3) = %d\n", A<<3);
       printf("Right shift output (B>>3) = %d\n", B>>3);	
return 0;
}	
