/*C program to find the volume of a cone*/
#include <stdio.h>
int main() {

	float h, r, volume;
	const float pi = 3.14;
	printf("Enter the height of the cone: ");
	scanf("%f",&h);
	printf("Enter the radius of the cone: ");
	scanf("%f",&r);
	volume=pi*r*r*h/3;

	printf("Volume of the cone is: %f\n", volume);

return 0;
}
